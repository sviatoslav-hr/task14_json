package com.khrystyna.dao.java.json;

import com.khrystyna.dao.java.model.Beer;

import java.io.File;
import java.util.List;

public class JSONUser {

  public static void main(String[] args) {
    File json = new File("src/main/resources/beerJSON.json");
    File schema = new File("src/main/resources/beerJSONScheme.json");

    JSONParser parser = new JSONParser();

    printList(parser.getBeerList(json));
  }

  private static void printList(List<Beer> beers) {
    System.out.println("JSON");
    for (Beer beer : beers) {
      System.out.println(beer);
    }
  }
}
