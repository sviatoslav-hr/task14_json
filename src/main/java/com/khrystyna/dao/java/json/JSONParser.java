package com.khrystyna.dao.java.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.khrystyna.dao.java.model.Beer;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JSONParser {
    private ObjectMapper objectMapper;

    public JSONParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Beer> getBeerList(File jsonFile){
        Beer[] beers = new Beer[0];
//        List<Beer> beers = new ArrayList<>();
        try{
            beers = objectMapper.readValue(jsonFile, Beer[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.asList(beers);
    }
}
