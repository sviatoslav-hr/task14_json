package com.khrystyna.views;

import com.khrystyna.comparator.BankComparator;
import com.khrystyna.controllers.Controller;
import com.khrystyna.model.Bank;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

public class BankView implements View {
    private static Logger logger = LogManager.getLogger(BankView.class);
    private Controller controller = new Controller();
    private File json = new File("src\\main\\resources\\bank\\banks.json");
    private File scheme = new File("src\\main\\resources\\bank\\banks.scheme.json");

    @Override
    public void run() {
        printList(controller
                .getBanks(json, scheme)
        );
    }

    private void printList(List<Bank> banks) {
        banks.sort(new BankComparator());
        for (Bank bank : banks) {
            logger.trace(bank);
        }
    }
}
