package com.khrystyna;

import com.khrystyna.views.BankView;

public class Application {
    /**
     * Main method that runs when the program is started.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        new BankView().run();
    }
}
