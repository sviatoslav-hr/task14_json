package com.khrystyna.controllers;

import com.khrystyna.model.Bank;
import com.khrystyna.service.BankService;

import java.io.File;
import java.util.List;

public class Controller {
    private BankService service = new BankService();

    public List<Bank> getBanks(File json, File scheme) {
        return service.getBanks(json,scheme);
    }
}
