package com.khrystyna.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.khrystyna.model.Bank;

import java.io.File;
import java.io.IOException;

public class JsonParser {
    private ObjectMapper objectMapper = new ObjectMapper();

    public Bank[] getBanks(File jsonFile) {
        try {
            return objectMapper.readValue(jsonFile, Bank[].class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
