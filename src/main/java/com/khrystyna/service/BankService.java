package com.khrystyna.service;

import com.khrystyna.json.JsonParser;
import com.khrystyna.json.JsonSchemeValidator;
import com.khrystyna.model.Bank;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BankService {
    private JsonParser parser = new JsonParser();
    private JsonSchemeValidator validator = new JsonSchemeValidator();

    public List<Bank> getBanks(File json, File scheme) {
        if (validator.validate(json, scheme)) {
            return Arrays.asList(parser.getBanks(json));
        } else {
            return Collections.emptyList();
        }
    }
}
